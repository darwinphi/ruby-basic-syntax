# Class names should be uppercase
class Greet
	def initialize(value)
		puts 'This is like a constructor in other language, ' + value
	end
	def three_times
		puts 'Hello World'
		puts 'Hello World'
		puts 'Hello World'
		name
	end

	def area_of_a_circle(radius)
		pi = 3.14
		area = pi * radius * radius
		return area
	end

	private
	def name
		puts 'Darwin'
	end

	
end
darwin = Greet.new('Darwin') 
darwin.three_times
# darwin.name
puts darwin.area_of_a_circle(2)