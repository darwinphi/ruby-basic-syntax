names = Array["Darwin", "Helen", "Azel"]

puts names
# puts names[0]
puts names.reverse
puts names.sort


friends = Array.new 
friends[0] = "John"
friends[5] = "Roman"
puts friends[0]
puts friends

puts friends.length
puts friends.include? "John"

puts 
puts 

a = [1, 2, 3, 4, 'Darwin']
puts a.class
puts a[0].class
puts a[4].class

puts 

b = Array.new 
b << "First Item"
b << "Second Item"
puts b

b.each do |item|
	puts item
end