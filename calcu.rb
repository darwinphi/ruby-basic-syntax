puts "Enter 1st number: "
# `chomp gets rid of the newline`
num1 = gets.chomp

puts "Enter 2nd number: "
num2 = gets.chomp

# `to_i` converts the string to integer
# `to_f` converts to float
sum = num1.to_f + num2.to_f
puts (sum)