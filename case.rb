def get_day(day)
	case day
		when "mon"
			day_name = "Monday"
		when "tue"
			day_name = "Tuesday"
		else
			day_name = "Invalid Day"
	end

	return day_name
end

puts get_day("mon")
puts get_day("monss")