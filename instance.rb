class SuperMath
	def pi
		3.14
	end

	# This is like the static method
	def self.static_pi
		3.14
	end
end

math = SuperMath.new 
puts math.pi

puts SuperMath.static_pi
