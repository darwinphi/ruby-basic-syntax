ismale = true 
istall = false

# can also use `or`

if ismale and istall
	puts "You are male"
elsif ismale and !istall
	puts "You are a dwarf male"
else
	puts "You are not male"
end

def max(num1, num2, num3) 
	# highestnum = ""
	if num1 > num2 and num1 > num3
		highestnum = num1
	elsif num2 > num1 and num2 > num3
		highestnum = num2
	else
		highestnum = num3
	end

	return "The highest number is " + highestnum.to_s
end

puts max(7, 2, 3)

###########################################################
# Shorthand ;)
a = 10
puts 'a is 10' if a == 10