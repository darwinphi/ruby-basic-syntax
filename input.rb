puts "What's your name? "
# `chomp gets rid of the newline`
name = gets.chomp

puts "What's your age? "
age = gets.chomp

puts ("Hello " + name + ", you are " + age + " years old")