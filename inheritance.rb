class Parent
	def surname
		return 'Manalo'
	end

	def middlename
		return 'Garino'
	end
end

class Child < Parent
	def name
		return 'Darwin ' + surname
	end
end

darwin = Child.new
puts darwin.name
puts darwin.middlename