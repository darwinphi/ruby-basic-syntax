countries = {
	"Philippines" => "PHI",
	"Indonesia" => "INA",
	"Vietnam" => "VIE",
	"Thailand" => "THA",
	"Singapore" => "SIN"
}

puts countries
puts countries["Philippines"]
puts countries.count

puts 
puts

# Symbols
name = "Darwin"
puts name.object_id
name = "Azel"
puts name.object_id
puts :name.class

puts 

#Using symbols as key is much more memory efficient
colors = { black: '#000000', white: '#FFFFFF' }
puts colors
puts colors[:white]

colors.each do |key, value|
	puts "#{key} #{value}"
end