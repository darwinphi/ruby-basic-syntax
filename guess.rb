secret_word = "boom"
guess_count = 0
guess_limit = 3
out_of_guesses = false

puts "What is the word? You have " + guess_limit.to_s + " guesses"
guess_word = gets.chomp

while guess_word != secret_word and !out_of_guesses
	guess_limit -= 1
	
	if guess_limit > 0
		puts "Enter guess: "
		puts "Guesses left: " + guess_limit.to_s
		guess = gets.chomp
	else
		out_of_guesses = true
	end		
end

if guess_word == secret_word
	puts "You guess it right"
end