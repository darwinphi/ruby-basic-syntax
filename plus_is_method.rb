# Aha! The plus symbol is indeed a method, and this method takes the next value as
# a parameter. Really, you should put this value in brackets, but thanks to Ruby’s well-
# thought- out syntax, this is not necessary.

puts 10 + 10
puts 10.+10
puts 10.+(10)

# Override that motherf**ker
class Integer
	def +(name, *args, &blk)
		42
	end
end

puts 10 + 12