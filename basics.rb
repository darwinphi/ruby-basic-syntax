# Displays the output in a newline
puts "Wassup"

# Displays the output in the same line
# print "Hello World "
# print "Darwin"

name = "Darwin"
puts ("Hi there " + name)

phrase = "Hello there Earthling!"
puts phrase
puts "helen".upcase
puts phrase.upcase
puts phrase.downcase
puts phrase.strip
puts phrase.length
puts phrase.include? "there"
puts phrase[0]
puts phrase[0, 4]
puts phrase.index("H")

puts 1 + 1
puts 2**3
puts 10 % 3

print "Hello"
print "World"