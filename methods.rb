def greet(name="No name", age="No age")
	puts "Hello, " + name + ", you are " + age.to_s
end

greet("Darwin", 18)

def cube(num)
	return num**3, 70
	puts "Hello"
end

puts cube(4)

def pow(base_num, pow_num)
	result = 1
	
	pow_num.times do
		result = result * base_num
	end

	return result
end

puts pow(2, 5)

class Wall
	def initialize
		# `@` It's like properties in other language
		@color = 'white'
	end

	def color
		@color
	end

	def paint(value)
		@color = value
	end
end

house = Wall.new 
puts house.color
puts house.paint('red')

class Roof
	def initialize
		@color = 'blue'
		@size = 5
	end

	# attr_reader :color, :size
	# attr_writer :color, :size

	# Combines getters and setters above ;)
	attr_accessor :color, :size
end

house2 = Roof.new 
puts house2.color
puts house2.size
puts
house2.color = 'pink'
house2.size = 69
puts "Writer: #{house2.color}"
puts "Writer: #{house2.size}"

# Automatically outputs string
class Person
	def initialize(first_name, last_name)
		@first_name = first_name
		@last_name = last_name
	end

	def to_s
		"#{@first_name} #{@last_name}"
	end
end

darwin = Person.new('Darwin', 'Manalo')
puts darwin